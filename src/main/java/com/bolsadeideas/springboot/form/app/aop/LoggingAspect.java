package com.bolsadeideas.springboot.form.app.aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;

import java.util.HashMap;
import java.util.Map;

public class LoggingAspect {
    private Logger logger = LogManager.getLogger("LogToFile");
    private StringBuilder sb = new StringBuilder();

    public void beforeAdvice(JoinPoint joinPoint) {
        if (logger.isDebugEnabled()) {
            this.sb.setLength(0);
            Object[] args = joinPoint.getArgs();
            Map<String, String> typeValue = new HashMap<>();
            for (Object obj : args) {
                if (obj != null) {
                    typeValue.put(obj.getClass().getName(), obj.toString());
                }
            }

            this.sb.append("[")
                    .append(joinPoint.getSignature().getDeclaringTypeName())
                    .append(", ")
                    .append(joinPoint.getSignature().getName())
                    .append(", ")
                    .append("Parameter:-> ")
                    .append(typeValue)
                    .append("]")
                    .append(" -BEGIN");

            logger.debug(this.sb.toString());
        }
    }

    public void afterAdvice(JoinPoint joinPoint) {
        if (logger.isDebugEnabled()) {
            this.sb.setLength(0);

            sb.append("[")
                    .append(joinPoint.getSignature().getName())
                    .append("]")
                    .append(" -END");

            logger.debug(this.sb.toString());
        }
    }

    public void afterReturningAdvice(JoinPoint joinPoint, Object result) {
        if (logger.isDebugEnabled() && result != null) {
            this.sb.setLength(0);
            this.sb.append("Method returned: [")
                    .append(joinPoint.getSignature().getName())
                    .append(", ")
                    .append("Result:-> ")
                    .append(result.getClass().getName())
                    .append(" --> ")
                    .append(result);

            logger.debug(this.sb.toString());
        }
    }

    public void afterThrowingAdvice(JoinPoint joinPoint, Throwable exception) {
        if (logger.isDebugEnabled()) {
            this.sb.setLength(0);
            this.sb.append("[")
                    .append(joinPoint.getSignature().getName())
                    .append("]")
                    .append(" - There has been an exception: ")
                    .append(exception.toString());

            logger.debug(this.sb.toString());
        }
    }
}
