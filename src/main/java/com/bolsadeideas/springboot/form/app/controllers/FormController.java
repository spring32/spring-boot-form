package com.bolsadeideas.springboot.form.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FormController {

    @GetMapping("/form")
    public ModelAndView form(ModelAndView mv) {
        mv.setViewName("formulario/form");

        return mv;
    }

    @PostMapping("/form")
    public ModelAndView procesarForm(ModelAndView mv) {
        mv.setViewName("formulario/resultado");

        return mv;
    }
}
